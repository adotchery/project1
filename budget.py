import tkinter as tk
import tkinter.ttk as ttk

root = tk.Tk()
root.title('Tip Calculator')
root.geometry('400x400')


class TipCalculator():
    def __init__(self):
        self.meal_cost = tk.StringVar()
        self.var1 = tk.StringVar()
        self.var2 = tk.StringVar()
        self.var3 = tk.StringVar()
        self.int1 = float(0.15)
        self.int2 = float(0.18)
        self.int3 = float(0.20)

# top label information
        top_label = ttk.Label(root,text="Enter Bill Total: ")
        top_label.pack()

# bill information entry
        meal_cost = ttk.Entry(root, textvariable = self.meal_cost)
        meal_cost.pack()

#button information
        button = ttk.Button(root, text="Calculate!", command=self.calculate)
        button.pack()

#lower frame with the value

        label1 = ttk.Label(root, textvariable = self.var1)
        label1.pack()


        label2 = ttk.Label(root, textvariable = self.var2)
        label2.pack()


        label3 = ttk.Label(root, textvariable = self.var3)
        label3.pack()


        root.mainloop()
# need a set if statement to > 0 if not go to the elif statement
    def calculate(self):
        if self.meal_cost.get().isnumeric():
            num1 = int(float(self.meal_cost.get()))
            num2 = int(float(self.meal_cost.get()))
            num3 = int(float(self.meal_cost.get()))
            new_num1 = num1*self.int1 + num1
            new_num2 = num2*self.int2 + num2
            new_num3 = num3*self.int3 + num3

            self.var1.set("15% tip is {}".format(new_num1))
            self.var2.set("18% tip is {}".format(new_num2))
            self.var3.set("20% tip is {}".format(new_num3))

        else:
            self.var1.set("Enter a Number greater than 0")
            self.var2.set("")
            self.var3.set("")

TipCalculator()
